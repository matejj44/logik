#include "Board.h"
#include "Hole.h"
#include "RatingHole.h"
#include "SubmitButton.h"
#include "Cap.h"
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
#include <QGraphicsView>

Board::Board(int w, int h){
    width = w;
    height = h;
    int board_w = (w/10)*7;
    int board_h = (h/20)*17;
    int hole_w = (board_w)/17;
    int hole_h = board_h/24;

    //colors
    QColor lightblue = QColor(217, 239, 246, 255);
    QColor darkblue = QColor(134, 168, 179, 255);
    QColor lightbrown = QColor(217, 202, 190, 255);
    QColor brown = QColor(175, 160, 147, 255);
    QColor darkbrown = QColor(66, 66, 66, 255);
    QColor browntrans = QColor(175, 160, 147, 200);

    //font
    QFont FontBold = QFont("futura", hole_h);
    FontBold.bold();

    QPen nobounds(Qt::transparent, 0);

    //scene
    scene = new QGraphicsScene();
    scene->setSceneRect(0, 0, w, h);

    //view
    QGraphicsView * view = new QGraphicsView();
    view->setScene(scene);
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setFixedSize(w, h);

    //background
    QGraphicsRectItem * background = new QGraphicsRectItem();
    background->setRect(0, 0, w, h);
    background->setBrush(lightblue);
    scene->addItem(background);

    //board
    QGraphicsRectItem * board = new QGraphicsRectItem();
    board->setRect((w-board_w)/2, (h-board_h)/2, board_w, board_h);
    board->setBrush(lightbrown);
    board->setPen(nobounds);
    scene->addItem(board);
    QGraphicsRectItem * boardshadow = new QGraphicsRectItem();
    boardshadow->setRect((w-board_w)/2, (h-board_h)/2 + board_h - board_h/12, board_w, board_h/12);
    boardshadow->setBrush(brown);
    boardshadow->setPen(nobounds);
    scene->addItem(boardshadow);
    view->show();

    for(int i = 0; i < 10; i++){

        //numbers
        QGraphicsTextItem* n = new QGraphicsTextItem();
        n->setPlainText(QString::number(10-i));
        n->setFont(FontBold);
        n->setDefaultTextColor(darkbrown);
        if (i == 0){
            n->setPos((w-board_w)*3/5-hole_h/4, (h-board_h)*7/6 + (board_h/13)*i);
        }
        else{
            n->setPos((w-board_w)*3/5, (h-board_h)*7/6 + (board_h/13)*i);
        }
        scene->addItem(n);
    }

    //separator
    QGraphicsRectItem* separator = new QGraphicsRectItem();
    separator->setRect (((w-board_w)*4/5 + (board_w/10)*4 + hole_w) + (((w-board_w)*4/5 + board_w*4/7)-((w-board_w)*4/5 + (board_w/10)*4 + hole_w))/2 - w/200, h - board_h, w/100 , board_h*4/5);
    separator->setBrush(brown);
    separator->setPen(nobounds);
    scene->addItem(separator);

    //game combination holes
    wincombination.resize(5);
    submitcombination.resize(5);
    srand(time(0));
    for(int j = 0; j < 5; j++){
        QGraphicsRectItem * combhole = new QGraphicsRectItem();
        combhole->setRect((w-board_w)*4/5 + (board_w/10)*j, (h-board_h)*11/14, hole_w, hole_h);
        //WTF??
        int color = rand() % 8;
            switch(color) {
                case 0:
                    combhole->setBrush(QColor(117, 179, 73, 255));
                    break;
                case 1:
                    combhole->setBrush(QColor(84, 144, 222, 255));
                    break;
                case 2:
                    combhole->setBrush(QColor(214, 81, 81, 255));
                    break;
                case 3:
                    combhole->setBrush(QColor(255, 208, 0, 255));
                    break;
                case 4:
                    combhole->setBrush(QColor(251, 251, 251, 255));
                    break;
                case 5:
                    combhole->setBrush(QColor(255, 119, 0, 255));
                    break;
                case 6:
                    combhole->setBrush(QColor(120, 120, 120, 255));
                    break;
                case 7:
                    combhole->setBrush(QColor(244, 174, 223, 255));
            }
        wincombination[j] = color;
        combhole->setPen(nobounds);
        scene->addItem(combhole);
       }

    //combination hiding cap
    Cap* cap = new Cap();
    cap->setRect((w-board_w)*29/40, (h-board_h)*2/3, hole_w*9, hole_h*2);
    scene->addItem(cap);
    QObject::connect(cap, &Cap::clicked, this, &Board::end);

    //holes
    holes.resize(50);
    rateholes.resize(50);
    int count = 0;
    for(int i = 0; i < 10; i++){

        //holes
        for(int j = 0; j < 5; j++){
            //player holes
            Hole * hole = new Hole(hole_w, hole_h);
            hole->setRect((w-board_w)*4/5 + (board_w/10)*j, (h-board_h)*5/4 + (board_h/13)*i, hole_w, hole_h);
            QObject::connect(hole, &Hole::wasClicked, this, &Board::raiseclickcounter);
            scene->addItem(hole);
            holes[count] = hole;

            //rating holes
            RatingHole * ratehole = new RatingHole(hole_w/2, hole_h/2);
            ratehole->setRect((w-board_w)*4/5 + board_w*4/7 + (board_w/18)*j, (h-board_h)*5/4 + (board_h/13)*i + hole_h/2, hole_w/2, hole_h/2);
            scene->addItem(ratehole);
            rateholes[count] = ratehole;
            count++;
        }
    }

    //submitbutton
    SubmitButton* submitbutton = new SubmitButton(w, h);
    scene->addItem(submitbutton);
    QObject::connect(this, &Board::allowsubmition, submitbutton, &SubmitButton::setClickable);
    QObject::connect(submitbutton, &SubmitButton::clicked, this, &Board::raiseattempts);

    for(int i = 0; i < 5; i++){
        holes[49-i]->setClickable();
    }
}

void Board::evaluate(std::vector<int> win, std::vector<int> submit){
    std::vector<int> a(5);
    std::vector<int> b(5);
    int black = 0;
    int white = 0;
    for (int i = 0; i < 5; i++){
        if(submit[i] == win [i]){
            black++;
            a[i] = 1;
            b[i] = 1;
        }
    }

    for (int i = 0; i < 5; i++){
        for (int j = 0; j < 5; j++){
            if((submit[i] == win[j]) & (a[i] != 1) & (b[j] != 1)){
                white++;
                a[i] = 1;
                b[j] = 1;
            }
        }
    }
    if (white + black > 5){
        white = 5 - black;
    }
    for (int j = 0; j < black; j++){
        rateholes[45-5*(attemptcounter-1)+j]->setBlack();
    }
    for (int j = 0; j < white; j++){
        rateholes[45-5*(attemptcounter-1)+black+j]->setWhite();
    }
    if (black == 5){
        victory = 1;
        this->end();
    }
    else if (attemptcounter == 10)
        this->end();
}

void Board::end(){
    QGraphicsRectItem* vyhra = new QGraphicsRectItem();
    vyhra->setRect(width/2-width/6, height/2 - height/8, width/3, height/4);
    vyhra->setBrush(QColor(217, 239, 246, 255));
    vyhra->setPen(QPen(QBrush(QColor(134, 168, 179, 255)), 5));
    scene->addItem(vyhra);
    QGraphicsTextItem* text = new QGraphicsTextItem();
    if (victory == 1){
        text->setPlainText("VICTORY!");
        }
    if(victory == 0)
        text->setPlainText("LOSS!");
    text->setFont(QFont("futura", height/16));
    text->setDefaultTextColor(QColor(66, 66, 66, 255));
    text->setPos(width/2 - text->boundingRect().width()/2, height/2 - height/12);
    scene->addItem(text);
    QGraphicsTextItem* attempts = new QGraphicsTextItem();
    attempts->setPlainText("number of attempts = " + QString::number(attemptcounter));
    attempts->setFont(QFont("futura", height/36));
    attempts->setDefaultTextColor(QColor(66, 66, 66, 255));
    attempts->setPos(width/2 - attempts->boundingRect().width()/2, height/2 + height/60);
    scene->addItem(attempts);
}


void Board::raiseclickcounter(){
    clickcounter++;
    if (clickcounter == 5){
        emit allowsubmition();
        clickcounter = 0;
    }
}

void Board::raiseattempts(){
    attemptcounter++;
    for (int i = 0; i < 5; i++){
        submitcombination[4-i] = holes[49-5*(attemptcounter-1)-i]->clr();
        holes[49-5*(attemptcounter-1)-i]->setNonClickable();
    }
    this->evaluate(wincombination, submitcombination);
    if((attemptcounter != 10) & (victory != 1)){
        for (int i = 0; i < 5; i++)
            holes[49-5*attemptcounter-i]->setClickable();
    }
}
