#include "Hole.h"
#include <QBrush>
#include <QGraphicsScene>
#include <QDebug>

Hole::Hole(int w, int h){
    //draw the rectangle
    setRect(0, 0, w, h);
    setPen(QPen(Qt::transparent, 0));
    setBrush(QColor(175, 160, 147, 255));
    setAcceptHoverEvents(false);
}

QColor Hole::colorswitch(int a){
    switch(a) {
    case 0:
        return (QColor(117, 179, 73, 255));
    case 1:
        return (QColor(84, 144, 222, 255));
    case 2:
        return (QColor(214, 81, 81, 255));
    case 3:
        return (QColor(255, 208, 0, 255));
    case 4:
        return (QColor(251, 251, 251, 255));
    case 5:
        return (QColor(255, 119, 0, 255));
    case 6:
        return (QColor(120, 120, 120, 255));
    case 7:
        return (QColor(244, 174, 223, 255));
    default:
        return(Qt::black);
    }
}

void Hole::mousePressEvent(QGraphicsSceneMouseEvent *event){
    if (clickable == 1){
        if (clicked == 0){
            emit wasClicked();
            clicked = 1;
        }
        if (color == 8)
            color = -1;
        color++;
        setBrush(colorswitch(color));
    }
}

void Hole::hoverEnterEvent(QGraphicsSceneHoverEvent *event){
    setOpacity(200);
    //qDebug() << "button was is hovered over";

}

void Hole::hoverLeaveEvent(QGraphicsSceneHoverEvent *event){
    setOpacity(100);
    //qDebug() << "button was not is hovered over";
}

int Hole::clr(){
    return color;
}

void Hole::setClickable(){
    clickable  = 1;
    setBrush(QColor(66, 66, 66, 255));
    setAcceptHoverEvents(true);
}

void Hole::setNonClickable(){
    clickable = 0;
    setBrush(colorswitch(color));
    setAcceptHoverEvents(false);
}
