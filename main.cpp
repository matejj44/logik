#include <QApplication>
#include <QGraphicsScene>

#include "Board.h"

using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    int w = 830;
    int h = 750;

    //board
    Board * board = new Board(w, h);

    return a.exec();
}
