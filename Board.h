#ifndef BOARD_H
#define BOARD_H

#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <vector>
#include "Hole.h"
#include "RatingHole.h"


class Board : public QObject, public QGraphicsRectItem{
    Q_OBJECT
public:
    Board(int w, int h);

public slots:
    void raiseclickcounter();
    void raiseattempts();
    void evaluate(std::vector<int> win, std::vector<int> submit);
    void end();

signals:
    void allowsubmition();

private:
    int clickcounter = 0;
    int attemptcounter = 0;
    int victory = 0;
    int width = 0;
    int height = 0;
    QGraphicsScene* scene;
    std::vector<Hole*> holes;
    std::vector<RatingHole*> rateholes;
    std::vector<int> wincombination;
    std::vector<int> submitcombination;
};

#endif // BOARD_H
