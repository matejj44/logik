#ifndef HOLE_H
#define HOLE_H

#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QColor>

class Hole : public QObject, public QGraphicsRectItem{
    Q_OBJECT
public:
    //constructor
    Hole(int w, int h);

    //public methods (event)
    QColor colorswitch(int a);
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void hoverEnterEvent(QGraphicsSceneHoverEvent * event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent * event);
    int clr();

public slots:
    void setClickable();
    void setNonClickable();

signals:
    int wasClicked();

private:
    int color = -1;
    int clickable = 0;
    int clicked = 0;

};

#endif // HOLE_H
