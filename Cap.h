#ifndef CAP_H
#define CAP_H

#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QString>

class Cap : public QObject, public QGraphicsRectItem{
    Q_OBJECT
public:
    //constructors
    Cap();

    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void hoverEnterEvent(QGraphicsSceneHoverEvent * event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent * event);
    void setClickable();
    void setNonClickable();

signals:
    void clicked();

private:
    int clickable = 0;
    int width = 0;
    int height = 0;
    QGraphicsTextItem* text;
};

#endif // CAP_H
