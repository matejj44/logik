#include "SubmitButton.h"
#include <QBrush>
#include <QGraphicsScene>
#include <QDebug>
#include <QGraphicsTextItem>

SubmitButton::SubmitButton(int w, int h){
    width = w;
    height = h;
    setRect(w/2-w/14, h*8/9, w/7, h/18);
    setPen(QPen(Qt::transparent, 0));

    text = new QGraphicsTextItem("SUBMIT", this);
    text->setFont(QFont("futura", h/36));
    text->setDefaultTextColor(Qt::transparent);
    text->setPos(w/2-text->boundingRect().width()/2, h*8/9 + h/36 - text->boundingRect().height()/2);
}

void SubmitButton::mousePressEvent(QGraphicsSceneMouseEvent *event){
    if (clickable == 1){
        emit clicked();
        this->setNonClickable();
    }
}

void SubmitButton::hoverEnterEvent(QGraphicsSceneHoverEvent *event){
    setBrush(QColor(217, 202, 190, 255));
}

void SubmitButton::hoverLeaveEvent(QGraphicsSceneHoverEvent *event){
    setBrush(Qt::white);
}

void SubmitButton::setClickable(){
    clickable = 1;
    setPen(QPen(QColor(175, 160, 147, 255), 2));
    setBrush(Qt::white);
    text->setDefaultTextColor(QColor(66, 66, 66, 255));
    setAcceptHoverEvents(true);
}

void SubmitButton::setNonClickable(){
    clickable = 0;
    setPen(QPen(Qt::transparent, 0));
    setBrush(Qt::transparent);
    text->setDefaultTextColor(Qt::transparent);
    setAcceptHoverEvents(false);
}
