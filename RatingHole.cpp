#include "RatingHole.h"
#include <QBrush>
#include <QGraphicsScene>
#include <QDebug>

RatingHole::RatingHole(int w, int h){
    setRect(0,0,w,h);
    setBrush(QColor(175, 160, 147, 255));
    setPen(QPen(Qt::transparent, 0));
}

void RatingHole::setBlack(){
    setBrush(Qt::black);
}

void RatingHole::setWhite(){
    setBrush(Qt::white);
}
