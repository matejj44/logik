#ifndef RATINGHOLE_H
#define RATINGHOLE_H

#include <QObject>
#include <QGraphicsRectItem>

class RatingHole : public QObject, public QGraphicsRectItem{
    Q_OBJECT
public:
    RatingHole(int w, int h);

public slots:
    void setBlack();
    void setWhite();
};

#endif // RATINGHOLE_H
