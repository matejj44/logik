#ifndef SUBMITBUTTON_H
#define SUBMITBUTTON_H

#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QString>

class SubmitButton : public QObject, public QGraphicsRectItem{
    Q_OBJECT
public:
    //constructors
    SubmitButton(int w, int h);

    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void hoverEnterEvent(QGraphicsSceneHoverEvent * event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent * event);
    void setClickable();
    void setNonClickable();

signals:
    void clicked();

private:
    int clickable = 0;
    int width = 0;
    int height = 0;
    QGraphicsTextItem* text;
};

#endif // SUBMITBUTTON_H
