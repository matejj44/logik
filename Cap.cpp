#include "Cap.h"
#include <QBrush>
#include <QGraphicsScene>
#include <QDebug>
#include <QGraphicsTextItem>

Cap::Cap(){
    setRect(0,0,1,1);
    setBrush(QColor(175, 160, 147, 255));
    setPen(QPen(Qt::transparent, 0));
    setAcceptHoverEvents(true);
    this->setClickable();
}

void Cap::mousePressEvent(QGraphicsSceneMouseEvent *event){
    if (clickable == 1){
        emit clicked();
        setBrush(Qt::transparent);
        this->setNonClickable();
    }
}

void Cap::hoverEnterEvent(QGraphicsSceneHoverEvent *event){
    setBrush(QColor(66, 66, 66, 255));
}

void Cap::hoverLeaveEvent(QGraphicsSceneHoverEvent *event){
    setBrush(QColor(175, 160, 147, 255));
}

void Cap::setClickable(){
    clickable = 1;
    setAcceptHoverEvents(true);
}

void Cap::setNonClickable(){
    clickable = 0;
    setAcceptHoverEvents(false);
}
